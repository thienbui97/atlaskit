#!/usr/bin/env bash
set -e

yarn config set progress false
yarn config set color always
yarn config set loglevel warn
