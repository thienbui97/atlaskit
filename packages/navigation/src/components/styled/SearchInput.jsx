import styled from 'styled-components';

const SearchInput = styled.input`
  border: 0;
  color: inherit;
  flex-grow: 1;
  font-size: 1.4em;
  outline: 0;
`;

SearchInput.displayName = 'SearchInput';
export default SearchInput;
