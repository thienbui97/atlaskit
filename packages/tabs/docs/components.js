const path = require('path');

module.exports = [
  { name: 'Tabs', src: path.join(__dirname, '../src/index.jsx') },
  { name: 'StatelessTabs', src: path.join(__dirname, '../src/Tabs.jsx') },
];
