const path = require('path');

module.exports = [
  { name: 'DropdownMenu', src: path.join(__dirname, '../src/index.jsx') },
  { name: 'Stateless DropdownMenu', src: path.join(__dirname, '../src/StatelessMenu.jsx') },
];
