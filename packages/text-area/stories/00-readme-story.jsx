import React from 'react';
import { storiesOf } from '@kadira/storybook';
import Readme from '@atlaskit/util-readme';

import { name, description } from '../package.json';

/* eslint-disable import/no-duplicates, import/first */
import defaultOverview from './examples/textarea-overview';
import defaultOverviewSource from '!raw!./examples/textarea-overview';
import defaultComponent from '../src/TextArea';
import defaultComponentSource from '!raw!../src/TextArea';
/* eslint-enable import/no-duplicates, import/first */

storiesOf(name, module)
  .add('📖 Text Area readme', () => (
    <Readme
      name={name}
      component={defaultComponent}
      componentSource={defaultComponentSource}
      example={defaultOverview}
      exampleSource={defaultOverviewSource}
      description={description}
    />
  ));
