import React from 'react';
import AkTextArea from '@atlaskit/text-area';

export default (
  <AkTextArea label="Text Area" placeholder="Free to play" />
);
