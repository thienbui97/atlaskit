import React from 'react';
import { shallow, mount } from 'enzyme';
import Base from '@atlaskit/field-base';
import sinon from 'sinon';

import { TextArea } from '../../src';

describe('text-area', () => {
  describe('properties', () => {
    describe('disabled prop', () => {
      it('should reflect its value to the TextArea', () => {
        expect(shallow(<TextArea disabled />)
          .find(Base).props().isDisabled).to.equal(true);
      });
    });

    describe('required prop', () => {
      it('should reflect its value to the TextArea', () => {
        expect(shallow(<TextArea required />)
          .find(Base).props().isRequired).to.equal(true);
      });
    });

    describe('isInvalid prop', () => {
      it('should reflect its value to the TextArea', () => {
        expect(shallow(<TextArea isInvalid />)
          .find(Base).props().isInvalid).to.equal(true);
      });
    });

    describe('spellCheck prop', () => {
      it('should render a textarea with a spellCheck prop', () => {
        expect(shallow(<TextArea isSpellCheckEnabled={false} />)
          .find(Base).childAt(0).props().spellCheck).to.equal(false);
      });
    });

    describe('invalidMessage prop', () => {
      it('should reflect its value to the TextArea', () => {
        expect(shallow(<TextArea invalidMessage="test" />)
          .find(Base).props().invalidMessage).to.equal('test');
      });
    });

    describe('rows and cols props', () => {
      it('should have 6 rows', () => {
        expect(shallow(<TextArea rows={6} />)
          .find(Base).childAt(0).prop('rows')).to.equal(6);
      });

      it('should have 100 columns', () => {
        expect(shallow(<TextArea cols={100} />)
          .find(Base).childAt(0).prop('cols')).to.equal(100);
      });
    });

    it('Input should have value="something"', () =>
      expect(shallow(<TextArea value="something" />)
        .find(Base).childAt(0).prop('value')).to.equal('something')
    );

    it('onChange should be called when input value changes', () => {
      const spy = sinon.spy();
      const wrapper = mount(<TextArea onChange={spy} />);
      wrapper.find('textarea').simulate('change');
      expect(spy.callCount).to.equal(1);
    });
  });
});
