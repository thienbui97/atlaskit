import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import TextArea from './TextArea';

export default class AkTextArea extends PureComponent {
  static propTypes = {
    /** The default value on component render. */
    value: PropTypes.string,
    /** Handler to be called after the value is updated. */
    onChange: PropTypes.func,
  }

  static defaultProps = {
    onChange: () => {},
  }

  state = {
    value: this.props.value,
  }

  handleOnChange = (e) => {
    this.setState({ value: e.target.value });
    this.props.onChange(e);
  }

  focus = () => {
    this.input.focus();
  }

  render() {
    return (
      <TextArea
        {...this.props}
        value={this.state.value}
        onChange={this.handleOnChange}
        ref={(input) => { this.input = input; }}
      />
    );
  }
}
