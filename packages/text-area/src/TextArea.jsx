import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import Base, { Label } from '@atlaskit/field-base';
import styled from 'styled-components';

const StyledTextArea = styled.textarea`
  background: transparent;
  border: 0;
  box-sizing: border-box;
  color: inherit;
  cursor: inherit;
  font-size: 14px;
  outline: none;
  width: 100%;
  resize: none;
  
  ::-ms-clear {
    display: none;
  }

  :invalid {
    box-shadow: none;
  }
`;

export default class TextArea extends PureComponent {
  static propTypes = {
    /** Sets the field as uneditable, with a changed hover state. */
    disabled: PropTypes.bool,
    /** Add asterisk to label. Set required for form that the field is part of. */
    required: PropTypes.bool,
    /** Sets styling to indicate that the input is invalid. */
    isInvalid: PropTypes.bool,
    /** Label to be displayed above the input. */
    label: PropTypes.string,
    /** Name value to be passed to the html input. */
    name: PropTypes.string,
    /** Text to display in the input if the input is empty. */
    placeholder: PropTypes.string,
    /** The value of the input. */
    value: PropTypes.string,
    /** Handler to be called when the input changes. */
    onChange: PropTypes.func.isRequired,
    /** Id value to be passed to the html input. */
    id: PropTypes.string,
    /** Sets whether to show or hide the label. */
    isLabelHidden: PropTypes.bool,
    /** Provided component is rendered inside a modal dialogue when the field is
    selected. */
    invalidMessage: PropTypes.node,
    /** Sets whether to apply spell checking to the content. */
    isSpellCheckEnabled: PropTypes.bool,
    /** Sets whether the component should be automatically focused*/
    autoFocus: PropTypes.bool,
    /** Set the maximum length that the entered text can be. */
    maxLength: PropTypes.number,
    /** Set the number of columns of the area. */
    cols: PropTypes.number,
    /** Set the number of rows of the text area. */
    rows: PropTypes.number,
  }

  static defaultProps = {
    disabled: false,
    required: false,
    isInvalid: false,
    isSpellCheckEnabled: true,
    cols: 50,
    rows: 4,
  }

  focus() {
    this.input.focus();
  }

  render() {
    return (
      <div>
        <Label
          label={this.props.label}
          htmlFor={this.props.id}
          isLabelHidden={this.props.isLabelHidden}
          isRequired={this.props.required}
        />
        <Base
          isDisabled={this.props.disabled}
          isInvalid={this.props.isInvalid}
          isRequired={this.props.required}
          invalidMessage={this.props.invalidMessage}
        >
          <StyledTextArea
            disabled={this.props.disabled}
            name={this.props.name}
            placeholder={this.props.placeholder}
            value={this.props.value}
            required={this.props.required}
            onChange={this.props.onChange}
            id={this.props.id}
            autoFocus={this.props.autoFocus}
            spellCheck={this.props.isSpellCheckEnabled}
            maxLength={this.props.maxLength}
            cols={this.props.cols}
            rows={this.props.rows}
            ref={(input) => { this.input = input; }}
          />
        </Base>
      </div>
    );
  }
}
