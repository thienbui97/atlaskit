import React from 'react';
import Button from '@atlaskit/button';
import Icon from '@atlaskit/icon/glyph/question';

export default (
  <Button
    iconAfter={<Icon />}
  >
    Info
  </Button>
);
