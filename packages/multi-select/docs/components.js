const path = require('path');

module.exports = [
  { name: 'Multi Select', src: path.join(__dirname, '../src/index.jsx') },
  { name: 'StatelessMultiSelect', src: path.join(__dirname, '../src/StatelessMultiSelect.jsx') },
];
