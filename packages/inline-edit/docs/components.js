const path = require('path');

module.exports = [
  { name: 'InlineEditor', src: path.join(__dirname, '../src/index.jsx') },
  { name: 'InlineEdit (stateless)', src: path.join(__dirname, '../src/InlineEdit.jsx') },
];
