import { style } from 'typestyle';
import { akGridSize } from '@atlaskit/util-shared-styles';

export const colorPalette = style({
  padding: `0 ${akGridSize}`
});
