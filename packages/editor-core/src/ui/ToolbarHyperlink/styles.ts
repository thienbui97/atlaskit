import { style } from 'typestyle';

export const outerContainer = style({
  position: 'relative'
});

export const textInputContainer = style({
  display: 'flex',
  alignItems: 'center',
  padding: '5px 10px'
});
