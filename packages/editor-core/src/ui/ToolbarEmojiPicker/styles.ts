import { style } from 'typestyle';

export const button = style({
  display: 'flex'
});

export const offsetY: number = 8; // Padding between picker and button is 8px
export const marginRight: number = 16; // Padding between right of editor and picker is 16px
